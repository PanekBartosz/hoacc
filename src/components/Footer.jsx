import React from 'react';
import {BiCopyright} from 'react-icons/bi';

const Footer = () => {
  return (
    <div className='bg-[#4A5859] mx-auto py-16 px-4 grid lg:grid-cols-2 gap-8 text-white'>
      <h1 className='w-full text-3xl text-right font-bold text-[white]'>HOACC <p className='text-[#7067CF]'>Home Accounting</p></h1>
      <h1 className='w-full text-3xl text-left font-bold text-[white]'>HOACC <p className='text-[#7067CF]'>Home Accounting</p></h1>
    </div>
  );
};

export default Footer;