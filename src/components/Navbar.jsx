import { useState } from 'react'
import React from 'react'
import {AiOutlineClose, AiOutlineMenu, AiOutlineBell} from 'react-icons/ai'
import {BiHomeAlt2, BiAddToQueue, BiHelpCircle} from 'react-icons/bi'
import {FiSettings} from 'react-icons/fi'

const Navbar = () => {

    const [nav, setNav] = useState(false)
    const handleNav = () => {
        setNav(!nav)
    }

  return (
    <div className='flex justify-between items-center h-24 max-w-[1240px] mx-auto px-4 text-[black]'>
        <h1 className='w-full text-3xl font-bold text-[black]'>HOACC</h1>
        <ul className='uppercase hidden md:flex'>
            <li className='p-4'>Home</li>
            <li className='p-4'>Add</li>
            <li className='p-4'>Notification</li>
            <li className='p-4'>Help</li>
            <li className='p-4'>Settings</li>
            <li className='p-4'><button className='bg-[#7067CF] w-[100px] rounded-md p-0 font-medium mx-auto text-black'>LOG IN</button></li>
        </ul>
        <div onClick={handleNav} className='block md:hidden'>
            {nav ? <AiOutlineClose size={25}/> : <AiOutlineMenu size={25} />}
        </div>
        <div className={nav ? 'fixed left-0 top-0 bg-[#ADB7A4] text-[black] w-[60%] border-r h-full border-r-gray-900 ease-in-out duration-500' : 'fixed left-[-100%]' }>
        <h1 className='w-full text-3xl font-bold m-4 text-[black]'>HOACC</h1>
            <ul className='uppercase p-4 '>
                <li className='p-4 border-b border-gray-600 flex'><BiHomeAlt2 size={25} className='mr-2'/>Home</li>
                <li className='p-4 border-b border-gray-600 flex'><BiAddToQueue size={25} className='mr-2'/>Add</li>
                <li className='p-4 border-b border-gray-600 flex'><AiOutlineBell size={25} className='mr-2'/>Notification</li>
                <li className='p-4 border-b border-gray-600 flex'><BiHelpCircle size={25} className='mr-2'/>Help</li>
                <li className='p-4 border-b border-gray-600 flex'><FiSettings size={25} className='mr-2'/>Settings</li>
                <li className='p-4'><button className='bg-[#7067CF] w-[180px] rounded-md p-3 font-medium mx-auto text-black'>LOG IN</button></li>
            </ul>
        </div>
    </div>
  )
}

export default Navbar