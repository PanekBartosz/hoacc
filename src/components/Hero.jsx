import React from 'react';
import Typed from 'react-typed';

const Hero = () => {
  return (
    <div className='text-white'>
      <div className='max-w-[800px] mt-[-96px] w-full h-screen mx-auto text-center flex flex-col justify-center'>
        <p className='text-[#7067CF] font-bold p-1'>
          HOME ACCOUNTING
        </p>
        <h1 className='md:text-7xl sm:text-6xl text-4xl font-bold md:py-6'>
        Efficient use of the budget.
        </h1>
        <div className='flex justify-center items-center'>
          <p className='md:text-5xl sm:text-4xl text-xl font-bold py-4'>
          <Typed strings={['Fast','Flexible','Transparent']} typeSpeed={100} backSpeed={120} loop/> accounting
          </p>
        </div>
        <p className='md:text-2xl text-xl font-bold text-gray-500'>Monitor spending data to increase efficiency in household budget management.</p>
        <button className='bg-[#7067CF] w-[200px] rounded-md font-medium my-6 mx-auto py-3 text-black'>Get Started</button>
      </div>
    </div>
  );
};

export default Hero;